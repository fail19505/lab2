## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №2<br>"Розширена робота з git"

## КВ-12 Гусельніков Антон

## Хід виконання роботи

## 1. Робота з віддаленим репозиторієм

### Зклонуємо репозиторій для ЛР2. Використовуємо абсолютний шлях до локальної папки з репозиторієм від ЛР1.

```
$ git clone file:///home/student/lab1/screenshot-to-code
Cloning into 'screenshot-to-code'...
remote: Enumerating objects: 526, done.
remote: Counting objects: 100% (526/526), done.
remote: Compressing objects: 100% (205/205), done.
remote: Total 526 (delta 322), reused 512 (delta 314)
Receiving objects: 100% (526/526), 163.07 KiB | 32.61 MiB/s, done.
Resolving deltas: 100% (322/322), done.
```

### Перевіримо, що в копії репозиторію ЛР2 у нас присутні гілки, які ми створювали для ЛР1:

```
$ git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/lab1-branch
  remotes/origin/main
```
### Перевіримо, куди "дивиться" наш поточний ремоут origin:

```
$ git remote -v
origin	file:///home/student/lab1/screenshot-to-code (fetch)
origin	file:///home/student/lab1/screenshot-to-code (push)
```
### Додамо ще один ремоут до нашої локальної копії:
```
$ git remote add upstream https://github.com/abi/screenshot-to-code
```
### І перевіримо список ремоутів:

```
$ git remote -v
origin	file:///home/student/lab1/screenshot-to-code (fetch)
origin	file:///home/student/lab1/screenshot-to-code (push)
upstream	https://github.com/abi/screenshot-to-code (fetch)
upstream	https://github.com/abi/screenshot-to-code (push)
```
### Отримаємо список змін з нового ремоута.
```
$ git fetch upstream
remote: Enumerating objects: 144, done.
remote: Counting objects: 100% (122/122), done.
remote: Compressing objects: 100% (26/26), done.
remote: Total 80 (delta 64), reused 70 (delta 54), pack-reused 0
Unpacking objects: 100% (80/80), 16.49 KiB | 1.27 MiB/s, done.
From https://github.com/abi/screenshot-to-code
 * [new branch]      color-thief -> upstream/color-thief
 * [new branch]      hosted      -> upstream/hosted
 * [new branch]      main        -> upstream/main
```

```
$ git branch -a
* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/lab1-branch
  remotes/origin/main
  remotes/upstream/color-thief
  remotes/upstream/hosted
  remotes/upstream/main
```

```
$  git checkout -b lab2-branch
Switched to a new branch 'lab2-branch'

$ git push
fatal: The current branch lab2-branch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin lab2-branch
```
### Щоразу при кожній команді push вказувати явно назву віддаленої копії

```
$ git push origin lab2-branch
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 2 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (7/7), 621 bytes | 621.00 KiB/s, done.
Total 7 (delta 3), reused 1 (delta 0)
To file:///home/student/lab1/screenshot-to-code
 * [new branch]      lab2-branch -> lab2-branch


$ git push
fatal: The current branch lab2-branch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin lab2-branch
```

### Зв'язати локальну гілку із конкретною віддаленою

```
$ git push -u origin lab2-branch
Branch 'lab2-branch' set up to track remote branch 'lab2-branch' from 'origin'.
Everything up-to-date

$ git push
Everything up-to-date
```
## Об'єднання станів (merge)

### Перевіримо поточний стан гілок:

```
$ git branch -a
* lab2-branch
  main
  remotes/origin/HEAD -> origin/main
  remotes/origin/lab1-branch
  remotes/origin/lab2-branch
  remotes/origin/main
  remotes/upstream/color-thief
  remotes/upstream/hosted
  remotes/upstream/main
```

```
$ git merge origin/lab1-branch 
Merge made by the 'recursive' strategy.
 1.txt | 0
 2.txt | 0
 3.txt | 0
 3 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 1.txt
 create mode 100644 2.txt
 create mode 100644 3.txt
```

```
$ git log --pretty=oneline --graph
*   06dc99c4bdb9f3fd41a310adce026220e8e241b3 (HEAD -> lab2-branch) Merge remote-tracking branch 'origin/lab1-branch' into lab2-branch
|\  
| * 3350d9f6c9ba07b7fc2540497e2700674c7bec81 (origin/lab1-branch) add 3.txt
| * e8b299700089aca4a2bf1ec06d326550a4889a76 add 2.txt
| * dcdb7b1dca46fe7c54878e7244e2936b66222bac add 1.txt
* | 47458dcd8daf88b7667486f5fcc4ca55623a3215 (origin/lab2-branch) add 33.txt
* | 23075529b986236d2699dd18f63f96942345bc08 add 22.txt
* | c562888c98d48f303a8944846ba8fb6c1eac320f add 11.txt
|/  
* 56e6f1f6085a8edb2523b24a47a1b53acff02921 (upstream/main, origin/main, origin/HEAD, main) show output settings at all times
* 2ad9780e80ccec37f1e38b570cec0d7e87a43017 minor visual improvements
* 58298a15e7bfbe114b3ed82f5d69477d8919a970 rename vanilla to "no framework"
* e2c1bd2537ef06062836206c1d87fae746389f7b improve UI for code output selection
* a839e4a239ae6b372ffccde0db17ef35ff579106 add a better display string for output settings
```

## Перенесення комітів до поточної гілки (cherry-pick)

```
$ git cherry-pick a6bcb63ee1bb10ba8bd07e1a79b27845a9b27a14
[lab2-branch cb3f890] add 222.txt
 Date: Wed Nov 29 18:20:27 2023 +0200
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 222.txt

$ git log --pretty=oneline --graph -n 10 --branches
* cb3f8903d7a933f27b0e881235e6fffb40c975a2 (HEAD -> lab2-branch) add 222.txt
| * 54e64e261d5f0dedc080932cb2d6e2ef5320954e (lab2-branch-2) add 333.txt
| * a6bcb63ee1bb10ba8bd07e1a79b27845a9b27a14 add 222.txt
| * 7a0fff379112aa90b7a8d3c6e09150394e6cebc4 add 111.txt
|/  
*   06dc99c4bdb9f3fd41a310adce026220e8e241b3 Merge remote-tracking branch 'origin/lab1-branch' into lab2-branch
|\  
| * 3350d9f6c9ba07b7fc2540497e2700674c7bec81 (origin/lab1-branch) add 3.txt
| * e8b299700089aca4a2bf1ec06d326550a4889a76 add 2.txt
| * dcdb7b1dca46fe7c54878e7244e2936b66222bac add 1.txt
* | 47458dcd8daf88b7667486f5fcc4ca55623a3215 (origin/lab2-branch) add 33.txt
* | 23075529b986236d2699dd18f63f96942345bc08 add 22.txt
```

## Визначення останнього спільного предка (merge-base)

```
$ git log --pretty=oneline --graph
* cb3f8903d7a933f27b0e881235e6fffb40c975a2 (HEAD -> lab2-branch) add 222.txt
*   06dc99c4bdb9f3fd41a310adce026220e8e241b3 Merge remote-tracking branch 'origin/lab1-branch' into lab2-branch
|\  
| * 3350d9f6c9ba07b7fc2540497e2700674c7bec81 (origin/lab1-branch) add 3.txt
| * e8b299700089aca4a2bf1ec06d326550a4889a76 add 2.txt
| * dcdb7b1dca46fe7c54878e7244e2936b66222bac add 1.txt
* | 47458dcd8daf88b7667486f5fcc4ca55623a3215 (origin/lab2-branch) add 33.txt
* | 23075529b986236d2699dd18f63f96942345bc08 add 22.txt
* | c562888c98d48f303a8944846ba8fb6c1eac320f add 11.txt
|/  
* 56e6f1f6085a8edb2523b24a47a1b53acff02921 (upstream/main, origin/main, origin/HEAD, main) show output settings at all times

$ git merge-base origin/lab2-branch origin/lab1-branch 
56e6f1f6085a8edb2523b24a47a1b53acff02921
```
## Ничка (stash)

### Зробимо трохи змін:
```
$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 5 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   11.txt
	modified:   22.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

### Збережемо ці зміни в ничку:
```
$ git stash
Saved working directory and index state WIP on lab2-branch: cb3f890 add 222.txt

$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 5 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

### Тепер можемо переглянути вміст нички:
```
$ git stash list
stash@{0}: WIP on lab2-branch: cb3f890 add 222.txt
```

### Щоби видобути зміни з нички можна скористатися git stash pop
```
$ git stash pop
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 5 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   11.txt
	modified:   22.txt

no changes added to commit (use "git add" and/or "git commit -a")
Dropped refs/stash@{0} (a76ce2549120a491cfca8839fca4fd483f90693a)
```

## Файл .gitignore

### Створимо кілька файлів з розширенням .kvfpm, а також файл .txt:
```
$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 6 commits.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	1.kvfpm
	2.kvfpm
	99.txt

nothing added to commit but untracked files present (use "git add" to track)
```
### Після зміни файлу .gitignore.
```
$ git status
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 6 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	99.txt

no changes added to commit (use "git add" and/or "git commit -a")
```


```
$  git status --ignored
On branch lab2-branch
Your branch is ahead of 'origin/lab2-branch' by 6 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	99.txt

Ignored files:
  (use "git add -f <file>..." to include in what will be committed)
	1.kvfpm
	2.kvfpm

no changes added to commit (use "git add" and/or "git commit -a")
```

```
$  git clean -fdx
Removing 1.kvfpm
Removing 2.kvfpm
Removing 99.txt
```

## Лог станів гілок (reflog)

### Коміти до видалення гілки
```
$ git log --pretty=oneline --graph -n 15
* fac9d56cbe7ff82ebd5a5bc667520716b20ce9e4 (HEAD -> lab2-branch) changes
* 21971ef372d9b74df6525cebdef33f54516ddc78 changes
*   06dc99c4bdb9f3fd41a310adce026220e8e241b3 Merge remote-tracking branch 'origin/lab1-branch' into lab2-branch
|\  
| * 3350d9f6c9ba07b7fc2540497e2700674c7bec81 (origin/lab1-branch) add 3.txt
| * e8b299700089aca4a2bf1ec06d326550a4889a76 add 2.txt
| * dcdb7b1dca46fe7c54878e7244e2936b66222bac add 1.txt
* | 47458dcd8daf88b7667486f5fcc4ca55623a3215 (origin/lab2-branch) add 33.txt
* | 23075529b986236d2699dd18f63f96942345bc08 add 22.txt
* | c562888c98d48f303a8944846ba8fb6c1eac320f add 11.txt
|/  
* 56e6f1f6085a8edb2523b24a47a1b53acff02921 (upstream/main, origin/main, origin/HEAD, main) show output settings at all times
* 2ad9780e80ccec37f1e38b570cec0d7e87a43017 minor visual improvements
* 58298a15e7bfbe114b3ed82f5d69477d8919a970 rename vanilla to "no framework"
* e2c1bd2537ef06062836206c1d87fae746389f7b improve UI for code output selection
* a839e4a239ae6b372ffccde0db17ef35ff579106 add a better display string for output settings
* 89db70dacac868e0e225ea1ddd7ff878fad8096b show sidebar on mobile and set proper z index
```
### Видалимо гілку lab2-branch, також видалимо її з серверу.
```
$ git branch -D lab2-branch
Deleted branch lab2-branch (was fac9d56).

$ git push -d origin lab2-branch
To file:///home/student/lab1/screenshot-to-code
 - [deleted]         lab2-branch
```

### Переглянемо reflog:
```
$ git reflog
0e9d6f9 (HEAD -> main) HEAD@{0}: commit: change .gitignore
56e6f1f (upstream/main, origin/main, origin/HEAD) HEAD@{1}: checkout: moving from lab2-branch to main
fac9d56 HEAD@{2}: commit: changes
21971ef HEAD@{3}: commit (amend): changes
cb3f890 HEAD@{4}: reset: moving to HEAD
cb3f890 HEAD@{5}: cherry-pick: add 222.txt
06dc99c HEAD@{6}: checkout: moving from lab2-branch-2 to lab2-branch
54e64e2 (lab2-branch-2) HEAD@{7}: commit: add 333.txt
a6bcb63 HEAD@{8}: commit: add 222.txt
7a0fff3 HEAD@{9}: commit: add 111.txt
06dc99c HEAD@{10}: checkout: moving from lab2-branch to lab2-branch-2
06dc99c HEAD@{11}: merge origin/lab1-branch: Merge made by the 'recursive' strategy.
47458dc HEAD@{12}: commit: add 33.txt
2307552 HEAD@{13}: commit: add 22.txt
c562888 HEAD@{14}: commit: add 11.txt
56e6f1f (upstream/main, origin/main, origin/HEAD) HEAD@{15}: checkout: moving from main to lab2-branch
56e6f1f (upstream/main, origin/main, origin/HEAD) HEAD@{16}: clone: from file:///home/student/lab1/screenshot-to-code
```

### Відновимо гілку:
```
$  git branch lab2-resurrected fac9d56

$  git checkout lab2-resurrected
Switched to branch 'lab2-resurrected'
```

### Впевнимось у відновленні:
```
$ git log --pretty=oneline --graph -n 15
* fac9d56cbe7ff82ebd5a5bc667520716b20ce9e4 (HEAD -> lab2-resurrected) changes
* 21971ef372d9b74df6525cebdef33f54516ddc78 changes
*   06dc99c4bdb9f3fd41a310adce026220e8e241b3 Merge remote-tracking branch 'origin/lab1-branch' into lab2-branch
|\  
| * 3350d9f6c9ba07b7fc2540497e2700674c7bec81 (origin/lab1-branch) add 3.txt
| * e8b299700089aca4a2bf1ec06d326550a4889a76 add 2.txt
| * dcdb7b1dca46fe7c54878e7244e2936b66222bac add 1.txt
* | 47458dcd8daf88b7667486f5fcc4ca55623a3215 add 33.txt
* | 23075529b986236d2699dd18f63f96942345bc08 add 22.txt
* | c562888c98d48f303a8944846ba8fb6c1eac320f add 11.txt
|/  
* 56e6f1f6085a8edb2523b24a47a1b53acff02921 (upstream/main, origin/main, origin/HEAD) show output settings at all times
* 2ad9780e80ccec37f1e38b570cec0d7e87a43017 minor visual improvements
* 58298a15e7bfbe114b3ed82f5d69477d8919a970 rename vanilla to "no framework"
* e2c1bd2537ef06062836206c1d87fae746389f7b improve UI for code output selection
* a839e4a239ae6b372ffccde0db17ef35ff579106 add a better display string for output settings
* 89db70dacac868e0e225ea1ddd7ff878fad8096b show sidebar on mobile and set proper z index
```